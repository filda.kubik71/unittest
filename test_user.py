from user import User

import unittest

class TestUser(unittest.TestCase):

    def test_user_activation(self):
        user1 = User()
        user1.activate()
        self.assertTrue(user1.is_active())
    
    def test_user_deactivation(self):
        user1 = user()

        user1.activate()
        self.assertTrue(user1.is_active())
        
        user1.deactivate()
        self.assertFalse(user1.is_active())

    def test_user_points_update(self):
        user1 = User()
        user1.add_points(25)
        self.assertEqual(user1.get_points(), 25)

    def test_user_level_change(self):
        user1 = User()
        user1.add_points(205)
        self.assertEqual(user1.get_level(), 1)

if __name__ == '__main__':
    unittest.main()

